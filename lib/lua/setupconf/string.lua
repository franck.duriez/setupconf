local _M = {}

_M.interpret_shell = function(text)
  if not text then return "" end
  local txt =
    text:gsub("^\"", "\\\""):gsub("([^\\])\"", "%1\\\"")
  local result = ""
  local first = true
  for line in io.popen(
    "echo \"" .. txt .. "\""):lines()
  do
    if first
    then first = false
    else result = result .. "\n"
    end

    result = result .. line
  end
  return result
end

_M.escape_lua = function(text)
  return text:gsub("\n", "\\\n"):gsub("\"", "\\\"")
end

return _M
