local _M = {}

local indent_value = 1

_M.indent = {
  size = 2,
  value = function() return indent_value end,
}

_M.indent.increase = function()
  indent_value = indent_value + 1
end

_M.indent.decrease = function()
  if indent_value > 1
  then
    indent_value = indent_value - 1
  end
end

local print_indent = function(char)
  io.write(string.rep(char, _M.indent.size * indent_value))
end

local level = 30

local level_to_int = {
  ["nothing"] = 0,
  ["error"]   = 10,
  ["warn"]    = 20,
  ["info"]    = 30,
  ["debug"]   = 40
}

_M.set_level = function(str)
  local lvl = level_to_int[str]
  if type(lvl) == "number"
  then level = lvl
  end
end

_M.info = function(text)
  if level < 30 then return end
  print_indent("-")
  io.write(" " .. text .. "\n")
end

_M.error = function(text)
  if level < 10 then return end
  print_indent("!")
  io.write(" " .. text .. "\n")
end

_M.warn = function(text)
  if level < 20 then return end
  print_indent("?")
  io.write(" " .. text .. "\n")
end

_M.debug = function(text)
  if level < 40 then return end
  print_indent(">")
  io.write(" " .. text .. "\n")
end

return _M
