local sc     = require("setupconf")
local scos   = require("setupconf.os")
local logger = require("setupconf.log")
local color  = require("setupconf.colorize")

local _M = {}

local sep = scos.path.separator

-- @brief Return the path of the given package.
_M.path = function(pack)
  if pack
  then
    return sc.path .. sep .. "packages" .. sep .. pack
  else
    return sc.path .. sep .. "packages"
  end
end

-- @brief Return an iterator on packages.
_M.list = function()
  local co = coroutine.create(function ()
    for dir in scos.ls(_M.path())
    do
      if scos.is_dir(_M.path(dir))
      then
        coroutine.yield(dir)
      end
    end
  end)

  return function()
    local code, res = coroutine.resume(co)
    return res
  end
end

_M.get_active_ones = function(active_str)

  local active_packages = {}

  for pack in string.gmatch(active_str, "%S+")
  do
    active_packages[pack] = true
  end

  return active_packages
end

_M.info_skip = function(package_name)
  logger.info("Skipping: " .. package_name)
end

_M.get_description = function(name)
  local filename = _M.path(name) .. sep .. "package.lua"
  local file = io.open(filename)
  if file
  then
    io.close(file)
    return dofile(filename)
  end
end

local preprocess_path = function(path, package)
  local res = string.gsub(path, "$ROOT", sc.path)
  res = string.gsub(res, "$PACROOT", sc.path ..
    sep .. "packages" .. sep .. package)
  res = string.gsub(res, "$HOME", os.getenv("HOME"))
  return res
end

local backup_file = function(path)
  local file = io.open(path, "r")
  if file
  then
    io.close(file)
    if scos.is_symlink(path)
    then
      os.remove(path)
      return false
    else
      local i = 1
      local backup = path .. ".backup"
      file = io.open(backup)
      while file
      do
        io.close(file)
        i = i+1
        backup = path .. ".backup" .. i
        file = io.open(backup)
      end
      os.rename(path, backup)
    end
    return true
  else
    return false
  end
end

local on_subpackage; on_subpackage = function(pack_name, subpack, conf)
  if type(subpack) == "string"
  then
    local name = pack_name .. sep .. subpack
    logger.info(color.colorize("Processing subpackage: %{bold:purple}" .. name .. "%{clear}"))
    logger.indent.increase()
    _M.setup(name , conf)
    logger.indent.decrease()
    logger.info("Subpackage processing terminated.")
    logger.info("Back to: " .. pack_name)
  else
    assert(type(subpack) == "table",
      "subpackage can only be a string or a table")
    for k,v in pairs(subpack)
    do
      on_subpackage(pack_name, v, conf)
    end
  end
end

local on = {
  mkdir = function(pack_name, mkdir, conf)
    for k,v in pairs(mkdir)
    do
      local path = preprocess_path(v, pack_name)
      logger.info("Create directory: " .. path)
      scos.mkdir(path)
    end
  end,
  copy = function(pack_name, copy, conf)
    for o,d in pairs(copy)
    do
      local orig = preprocess_path(o, pack_name)
      local dest = preprocess_path(d, pack_name)
      if scos.diff(orig, dest)
      then
        if backup_file(dest)
        then
          logger.info("Backup: " .. dest)
        end
        logger.info("Copy file: " .. orig .. " -> " .. dest)
        scos.cp(orig, dest)
      else
        logger.info("File already installed: " .. dest)
      end
    end
  end,
  copy_if_not_exists =
  function(pack_name, copy_if_not_exists, conf)
    for o,d in pairs(copy_if_not_exists)
    do
      local orig = preprocess_path(o, pack_name)
      local dest = preprocess_path(d, pack_name)
      if not scos.exists(dest)
      then
        logger.info("Copy file: " .. orig .. " -> " .. dest)
        scos.cp(orig, dest)
      else
        logger.info("File already exist: " .. dest)
      end
    end
  end,
  link = function(pack_name, link, conf)
    for o,d in pairs(link)
    do
      local orig = preprocess_path(o, pack_name)
      local dest = preprocess_path(d, pack_name)
      if backup_file(dest)
      then
        logger.info("Backup: " .. dest)
      end
      logger.info("Link file: " .. orig .. " -> " .. dest)
      scos.ln(orig, dest)
    end
  end,
  subpackage = on_subpackage
}

local processed_packages = {}

local setup; setup = function(name, description, conf)
  if not processed_packages[name] and description
  then
    for k,v in pairs(description)
    do
      if type(k) == "number"
      then
        setup(name, v, conf)
      else
        assert(type(k) == "string", "Impossible happened...")
        local fn = on[k]
        if fn
        then
          fn(name, v, conf)
        else
          logger.log(logger.error, "Invalid package key: " .. k)
        end
      end
    end
  else
    if processed_packages[name]
    then
      logger.info("Package already processed as dependency...")
    end
  end
end

on.run = function(pack_name, fn, conf)
  logger.info(color.colorize("Launching package %{bold:green}run%{clear} function."))
  local ret = fn(conf, _M.path(pack_name))
  if ret then setup(pack_name, ret, conf) end
end

on.depends = nil
on.depends = function(pack_name, deps, conf)
  if type(deps) == "string"
  then
    if processed_packages[deps] then return end
    local descr = _M.get_description(deps)
    logger.info(color.colorize("%{bold:blue}Processing dependency%{clear}: %{bold:yellow}" .. deps .. "%{clear}"))
    logger.indent.increase()
    setup(deps, descr, conf)
    processed_packages[deps] = true
    logger.indent.decrease()
    logger.info(color.colorize("%{blue}Dependency processing terminated%{clear}: " .. deps))
  else
    assert(type(deps) == "table", "Impossible happened...")
    for k,v in pairs(deps)
    do
      on.depends(pack_name, v, conf)
    end
  end
end

_M.setup = function(pack, conf)
  -- Get name and description.
  local name, description = "", ""
  if type(pack) == "table"
  then
    name = pack["name"]
    description = pack["description"]
    assert(name and description,
      "package must contain at least a name " ..
      "and descrption field.")
  else
    assert(type(pack) == "string",
      "package must be either a table or a string")
    name = pack
    description = _M.get_description(name)
  end

  setup(name, description, conf)
  processed_packages[name] = true
end


return _M
