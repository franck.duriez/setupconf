local _M = {}

local color_dict = {
  ["black"]  = 0,
  ["red"]    = 1,
  ["green"]  = 2,
  ["yellow"] = 3,
  ["blue"]   = 4,
  ["purple"] = 5,
  ["cyan"]   = 6,
  ["white"]  = 7,
}

local pattern_dict = {
  ["bold"] = "[1;3%m",
  ["norm"] = "[0;3%m",
  ["bgrd"] = "[4%m",
  ["high"] = "[0;9%m",
  ["hgbd"] = "[1;9%m",
  ["hgbg"] = "[0;10%m",
  ["undl"] = "[4;3%m",
}

local special_dict = {
  ["bold"]  = "[1m",
  ["clear"] = "[0m"
}

local match_color = function(colors)
  return string.gsub(colors, "([a-z:]+)|?", _M.color)
end

_M.colorize = function(str)
  return string.gsub(str, "%%{([a-z:|]+)}", match_color)
end

local apply_pattern = function(pattern, cl)
  local pt = pattern_dict[pattern]
  if cl and pt then return string.gsub(pt, "%%", cl) end
  return ""
end

local apply_pattern2 = function(pattern, color)
  local cl = color_dict[color]
  if cl then return apply_pattern(pattern, cl) end
  return ""
end

_M.color = function(...)
  local args = {...}
  local res = ""
  for _,arg in pairs(args)
  do
    local color = color_dict[arg]
    if color
    then
      res = res .. string.char(27) .. apply_pattern("norm", color)
    else
      local patt = string.gsub(arg, "(%w+):(%w+)", apply_pattern2)
      if patt ~= arg
      then
        res = res .. string.char(27) .. patt
      else
        local spec = special_dict[arg]
        res = res .. string.char(27) .. spec
      end
    end
  end
  return res
end

return _M