local scstr = require("setupconf.string")
local _M = {}

local print_indent = function(indent, indent_size)
  return string.rep(" ", indent * indent_size)
end

local print_handler = {}
local get_print_handler = function(elem)
  return print_handler[type(elem)] or print_handler.default
end

print_handler.default = function(
  elem, start, indent, indent_size)
  local str = ""
  if start then str = print_indent(indent, indent_size) end
  str = str .. "[:" .. type(elem) .. ":]"
  coroutine.yield(str, true)
end

print_handler.string = function(
  elem, start, indent, indent_size)
  local str = ""
  if start then str = print_indent(indent, indent_size) end
  str = str .. "\"" .. scstr.escape_lua(elem) .. "\""
  coroutine.yield(str, true)
end

print_handler.boolean = function(
  elem, start, indent, indent_size)
  local str = ""
  if start then str = print_indent(indent, indent_size) end
  if elem
  then str = str .. "true"
  else str = str .. "false"
  end
  coroutine.yield(str, true)
end

print_handler.number = function(
  elem, start, indent, indent_size)
  local str = ""
  if start then str = print_indent(indent, indent_size) end
  str = str .. elem
  coroutine.yield(str, true)
end

print_handler.table = function(
  elem, start, indent, indent_size)
  local str = ""
  if start then str = print_indent(indent, indent_size) end
  str = str .. "{"
  coroutine.yield(str, false)
  for k,v in pairs(elem)
  do
    if type(k) == "number"
    then
      local co = coroutine.create(get_print_handler(v))
      while true
      do
        local errfree, str, finish = coroutine.resume(
          co, v, true, indent+1, indent_size)
        if not errfree or coroutine.status(co) == "dead"
        then break end
        if finish
        then coroutine.yield(str .. ",", false)
        else coroutine.yield(str, false)
        end
      end
    else
      local first = true
      local co = coroutine.create(get_print_handler(v))
      while true
      do
        local errfree, str, finish = coroutine.resume(
          co, v, false, indent+1, indent_size)
        if not errfree or coroutine.status(co) == "dead"
        then break end
        if first
        then
          first = false
          str = print_indent(indent+1, indent_size) ..
            "\"" .. k .. "\": " .. str
        end
        if finish
        then coroutine.yield(str .. ",", false)
        else coroutine.yield(str, false)
        end
      end
    end
  end
  str = print_indent(indent, indent_size) .. "}"
  coroutine.yield(str, true)
end

local printer = function(elem, indent, indent_size, prnt)
  local co = coroutine.create(get_print_handler(elem))
  while true
  do
    local errfree, str, finish = coroutine.resume(
      co, elem, false, indent, indent_size)
    if not errfree or coroutine.status(co) == "dead"
    then break end
    prnt(str)
  end
end

_M.print = function(elem, prnt, indent_size)
  if not indent_size then indent_size = 2 end
  if not prnt then prnt = print end
  printer(elem, 0, indent_size, prnt)
end

return _M
