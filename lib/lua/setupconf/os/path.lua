local _M = {}

_M.separator = "/"

function _M.absolute(path)
  if string.sub(path, 1, 1) == '/' then return path end
  return (os.getenv("PWD") or '') .. '/' .. path
end

return _M
