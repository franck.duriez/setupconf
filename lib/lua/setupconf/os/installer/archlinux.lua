local logger = require("setupconf.log")

local _install_one = function(pack)
    for line in io.popen("/bin/pacman -Qn " .. pack .. " 2>/dev/null"):lines()
    do
      logger.info("System package already installed: " .. line)
      return false
    end
    local ok, err, ret = os.execute("/bin/sudo /bin/pacman --noconfirm -S " .. pack)
    return ret == 0
  end

return {
  install_one = _install_one,
  install = function(packs)
    if (type(packs) == string)
    then
      _install_one(packs)
    else
      assert(type(packs) == "table",
        "packages must be either a string or a table.")
      for k,v in pairs(packs)
      do
        _install_one(v)
      end
    end
  end
}
