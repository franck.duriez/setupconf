local path = require("setupconf.os.path")

local _M = {path = path}

_M.ls = function(path)
  local count, result = 0, {}
  for filename in io.popen('/bin/ls -A "' .. path .. '"'):lines()
  do
    count = count + 1
    result[count] = filename
  end

  local current = 0
  return function()
    if current < count
    then
      current = current + 1
      return result[current]
    end
  end
end

_M.mkdir = function(path)
  os.execute("/bin/mkdir -p " .. path)
end

_M.is_dir = function(path)
  for line in io.popen("/bin/ls -dAl " .. path .. " 2>/dev/null"):lines()
  do
    return string.sub(line,1,1) == 'd'
  end
end

_M.is_file = function(path)
  for line in io.popen("/bin/ls -dAl " .. path .. " 2>/dev/null"):lines()
  do
    return string.sub(line,1,1) == '-'
  end
  return false
end

_M.diff = function(path1, path2)
  for line in io.popen("/usr/bin/diff \"" .. path1 .. "\" \"" .. path2 .. "\""):lines()
  do
    return true
  end
  return false
end

_M.is_symlink = function(path)
  for line in io.popen("/bin/ls -dAl " .. path .. " 2>/dev/null"):lines()
  do
    return string.sub(line,1,1) == 'l'
  end
  return false
end

_M.ln = function(origin, destination)
  if _M.is_symlink(destination)
  then
    os.remove(destination)
  end
  os.execute("/bin/ln -s '" .. origin .. "' '" .. destination .. "'")
end

_M.cp = function(origin, destination)
  os.execute("/bin/cp -r '" .. origin .. "' '" .. destination .. "'")
end

_M.exists = function(path)
  return os.rename(path, path)
end

_M.type = function()
  -- Test is archlinux.
  local desc = io.open("/etc/arch-release")
  if desc then io.close(desc); return "archlinux" end
  -- Test if ubuntu.
  ---- TODO
  -- Test if cygwin.
  ---- TODO
end

_M.which = function(prog)
  for line in io.popen("which " .. prog .. " 2>/dev/null"):lines()
  do
    return line
  end
  return false
end

_M.lscpu = function()
  local result = {}
  for line in io.popen("lscpu"):lines()
  do
    local key, value = string.match(line, "^ *([^:]*): *(.*)$")
    result[key] = value
  end
  return result
end

_M.wget = function(link, path, prt)
  local proc = io.popen("wget \"" .. link .. "\" -O \"" .. path .. "\" 2>&1")
  for line in proc:lines()
  do
    if prt then prt(line) end
  end
  return proc:close()
end

_M.mime_type = function(file, archive)
  local cmd
  if not archive
  then
    cmd = "file --brief --mime-type "
  else
    cmd = "file --brief --uncompress --mime-type "
  end

  for line in io.popen(cmd .. file):lines()
  do
    return line
  end
end

local extract_by_mime = {
  ["application/x-bzip2"] = function(file)
    if _M.mime_type(file, true) == "application/x-tar"
    then return "tar xvjf "
    else return "bunzip2 "
    end
  end,
  ["application/x-gzip"] = function(file)
    if _M.mime_type(file, true) == "application/x-tar"
    then return "tar xvzf "
    else return "gunzip "
    end
  end,
  ["application/x-xz"] = function(file)
    if _M.mime_type(file, true) == "application/x-tar"
    then return "tar xvJf "
    else return "unxz "
    end
  end,
  ["application/x-compress"] = function(file)
    if _M.mime_type(file, true) == "application/x-tar"
    then return "tar xvZf "
    else return "uncompress "
    end
  end,
  ["application/x-7z-compressed"]  = "7za x ",
  ["application/x-rar-compressed"] = "unrar ",
  ["application/x-tar"]            = "tar xvf ",
  ["application/zip"]              = "unzip ",
}

extract_by_mime["application/gzip"] = extract_by_mime["application/x-gzip"]

local extract_command = function(file)
  local mime = _M.mime_type(file, false)
  local cmd = extract_by_mime[mime]
  if type(cmd) == "function" then return cmd(file) else return cmd end
end

_M.extract = function(tarball, outpath, prt)
  local cmd = extract_command(tarball)
  if not cmd
  then
    return "No handler for this archive type"
  end

  local proc = io.popen("cd " .. outpath .. " && " .. cmd .. path.absolute(tarball))
  for line in proc:lines()
  do
    if prt then prt(line) end
  end
end

return _M
