local scos = require("setupconf.os")
local _M = {}

_M.path = os.getenv("SETUPCONF_PATH")

local sep = scos.path.separator
_M.profile_path = _M.path .. sep .. "user-profile.sh"

_M.parse_profile = function()
  local profile_file = io.open(_M.profile_path, "r")
  local result = {}

  if profile_file
  then
    io.close(profile_file)
    for line in io.lines(_M.profile_path)
    do
      local key, value = string.match(line, "^ *var_set  *([^ ]*)  *(.*) *$")

      if key and value
      then
        local len = string.len(value)
        local first = string.sub(value,1,1)
        local last = string.sub(value,len,len)
        if first == last and (first == "\"" or first == "'")
        then
          value = string.sub(value, 2, len-1)
        end
        result[key] = value
      end
    end
    return true, result
  else
    return false, result
  end
end

_M.create_default_profile = function()
  local profile_file = io.open(_M.profile_path, "w")

  profile_file:write(
[[#!/bin/sh

# Set your name here.
var_set setupconf_name ''

# Set your mail here.
var_set setupconf_mail ''

# Set your prefered editor here (emacs, nano, vim, vi).
var_set setupconf_editor ''

# Set active packages.
# Packages are present in the $SETUPCONF_PATH/packages directory.
var_set setupconf_packages ''

# Set installation type (system or local).
var_set setupconf_install 'local'

# Choose your zsh prompt
# Choice are:
#   basic adam1 adam2 bart zefram fade redhat
#   suse walters bigfade clint elite elite2
#   fire off olivier
var_set setupconf_zprompt 'basic'
]])

  io.close(profile_file)

  return {
    setupconf_name     = '',
    setupconf_mail     = '',
    setupconf_editor   = '',
    setupconf_packages = '',
    setupconf_install  = 'local',
    setupconf_zprompt  = 'basic'
  }
end

return _M
