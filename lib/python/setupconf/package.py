import os
import setupconf


def path(pack_name=None):
  if pack_name:
    return os.path.join(setupconf.path, "packages", pack_name)
  else:
    return os.path.join(setupconf.path, "packages")
