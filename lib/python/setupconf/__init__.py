import os, sys
import re


path = os.environ["SETUPCONF_PATH"]
profile_path = os.path.join(path, "user-profile.sh")

def parse_profile():
  result = {}

  if os.path.isfile(profile_path):
    with open(profile_path, "r") as f:
      for line in f.readlines():
        words = re.match(r"^ *var_set +([^ ]+) +(.*) *$", line)
        if words:
          key   = words.group(1)
          value = words.group(2)
          if value[0] in [ '"', "'" ] and value[0] == value[-1]:
            value = value[1:-1]
          result[key] = value

  return result
