# setupconf

Quick configuration installation.
Inspired greatly from https://github.com/cgestes/ctafconf

## Description

This tool is meant for quick install of configuration (ex: git, vim or zsh)
on a machine.

You can create custom packages and put it in the packages directory to use it.

## Usage

* Clone it where you want (recommanded: $HOME/.config).
* Run bin/setup-config
* Fill user-profile.sh
* Run bin/setup-config again.
* It's done.
