local setupconf = require("setupconf")
local logger    = require("setupconf.log")

return {
  link = {
    ["$PACROOT/etc/user-dirs.dirs"] = "$HOME/.config/user-dirs.dirs",
    ["$PACROOT/etc/user-dirs.locale"] = "$HOME/.config/user-dirs.locale",
  },
  run = function(conf)
    local filename = os.getenv("HOME") .. "/.setupconf"
    local file = io.open(filename, "w")
    if file
    then
      logger.info("Create file: " .. filename)
      file:write("export SETUPCONF_PATH=" .. setupconf.path .. "\n")
      file:close()
    else
      logger.error("Unable to write in file: " .. filename)
    end
  end
}
