#!/usr/bin/env perl
#
# Some useful color codes, see end of file for more.
#
$col_black      = "\033[30m";
$col_red        = "\033[31m";
$col_green      = "\033[32m";
$col_brown      = "\033[33m";
$col_blue       = "\033[34m";
$col_purple     = "\033[35m";
$col_cyan       = "\033[36m";
$col_white      = "\033[37m";
$col_ltgray     = "\033[37m";
$col_norm       = "\033[00m";
$col_background = "\033[07m";
$col_brighten   = "\033[01m";
$col_underline  = "\033[04m";
$col_blink      = "\033[05m";

$col_default     = $col_norm;
$col_gcc         = $col_brigthen . $col_purple ;
$col_make        = $col_brighten . $col_cyan;
$col_filename    = $col_brighten . $col_brown;
$col_linenum     = $col_brighten . $col_cyan;
$col_trace       = $col_norm . $col_cyan;
$col_warning     = $col_brighten . $col_green;
$col_error       = $col_brighten . $col_red;
$error_highlight = $col_brighten;

sub is_interactive
{
  return 1;
  return -t STDOUT;
}

$| = 1;
while (<>)
{
  if (!is_interactive())
  {
    print $_;
    next;
  }

  $orgline = $thisline = $_;

  # Remove multiple spaces
  $thisline =~ s/  \+/ /g;

  #filename(123): warning: ...
  $thisline =~ s|^(.*:\s*)(\(\d+\)\s*:\s*)(error\s*:\s+)(.*)|$col_filename$1$col_linenum$2$col_underline$col_error$3$col_norm$col_purple$4$col_default|x;
  $thisline =~ s|^(.*)(\(\d+\)\s*:\s*)(warning\s*:\s+)(.*)|$col_filename$1$col_linenum$2$col_underline$col_warning$3$col_norm$col_brighten$col_purple$4$col_default|x;

  #filename:123: warning: ...
  $thisline =~ s|^(.*:\s*)(\d+\s*:\s*)(error\s*:\s+)(.*)|$col_filename$1$col_linenum$2$col_underline$col_error$3$col_norm$col_purple$4$col_default|x;
  $thisline =~ s|^(.*)(\d+\s*:\s*)(warning\s*:\s+)(.*)|$col_filename$1$col_linenum$2$col_underline$col_warning$3$col_norm$col_brighten$col_purple$4$col_default|x;

  print $thisline;
}

if (is_interactive())
{
  print $col_default;
}

