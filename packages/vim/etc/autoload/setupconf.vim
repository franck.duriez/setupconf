function! setupconf#load_packages()
  if has("unix")
     let s:path_sep = "/"
  else
     let s:path_sep = "\\"
  endif

  let s:setupconf_path_file = expand('$HOME') . s:path_sep . ".setupconf"
  for line in readfile(s:setupconf_path_file)
    if line =~ '^export SETUPCONF_PATH='
      let s:setupconf_path = substitute(line, "export SETUPCONF_PATH=", "", "")
    endif
  endfor
  let s:profile_file = s:setupconf_path . s:path_sep . "user-profile.sh"
  let s:lines = readfile(s:profile_file)
  let s:prof_line = ""
  for line in s:lines
    if line =~ '^var_set setupconf_packages'
      let s:prof_line = line
      break
    endif
  endfor
  let s:pack_str = substitute(s:prof_line, "var_set setupconf_packages" , "", "")
  let s:pack_str = substitute(s:pack_str, '"', "", "g")
  let s:pack_str = substitute(s:pack_str, "'", "", "g")
  let s:packages = split(s:pack_str)
  for package in s:packages
    let s:package_path =  s:setupconf_path .
                        \ s:path_sep .
                        \ "packages"  .
                        \ s:path_sep .
                        \ package .
                        \ s:path_sep .
                        \ "vim"
    call pathogen#infect(s:package_path)
  endfor
endfunction

