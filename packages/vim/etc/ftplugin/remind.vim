if exists('b:loaded_ftplugin_remind_user')
    finish
endif
let b:loaded_ftplugin_remind_user = 1

" Indent with 2 spaces
setlocal expandtab
setlocal smarttab
setlocal shiftwidth=2
setlocal tabstop=2
setlocal softtabstop=2
setlocal foldmarker=#<,#>
setlocal foldmethod=marker
