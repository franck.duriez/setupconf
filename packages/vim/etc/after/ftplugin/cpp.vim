" OmniCppComplete initialization
call omni#cpp#complete#Init()

if !exists('loaded_snippet') || &cp
    finish
endif

Snippet if if (<{}>)<CR>{<CR><{}><CR>}
Snippet else else<CR>{<CR><{}><CR>}
Snippet ife if (<{}>)<CR>{<CR><{}><CR>}<CR>else<CR>{<CR><{}><CR>}
Snippet switch switch (<{}>)<CR>{<CR>case <{}>:<CR>{<CR><{}><CR>}<CR><CR>case <{}>:<CR>{<CR><{}><CR>}<CR>}

Snippet nsp namespace <{}>{<CR><{}><CR>}
Snippet cls class <{}><CR>{<CR>public:<CR><{}><CR>};
Snippet stc struct <{}><CR>{<CR><{}><CR>};
Snippet unn union <{}><CR>{<CR><{}><CR>};
