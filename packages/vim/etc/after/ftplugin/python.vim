" Indent with 2 spaces
setlocal expandtab
setlocal smarttab
setlocal shiftwidth=2
setlocal tabstop=2
setlocal softtabstop=2

if !exists('loaded_snippet') || &cp
    finish
endif

Snippet . self.<{}>

Snippet ifn if __name__ == "__main__":<CR><{}>

Snippet class class <{}>:<CR><CR>def <{}>:<CR>

Snippet _init def __init__(self<{}>):<CR><{}>

Snippet _str  def __str__(self):<CR><{}>

Snippet pdb import pdb; pdb.set_trace()
Snippet ipdb import ipdb; ipdb.set_trace()

