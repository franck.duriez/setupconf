
return {
  { link = {
    ["$PACROOT/etc"]       = "$HOME/.vim",
    ["$PACROOT/etc/vimrc"] = "$HOME/.vimrc",
  }},
  { copy_if_not_exists = {
    ["$PACROOT/etc/vimrc.user"] = "$HOME/.vimrc.user",
  }},
  { mkdir = {
    "$HOME/.vim/backup"
  }},
}

