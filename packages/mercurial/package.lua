local scos = require("setupconf.os")

local sep = scos.path.separator

return {
  run = function(profile, pack_path)
    local hgrc = io.open(os.getenv("HOME") .."/.hgrc", "w")

    -- UI section
    hgrc:write("[ui]\n")
    hgrc:write("username = " .. profile["setupconf_name"] ..
      " <" .. profile["setupconf_mail"] .. ">\n")
    hgrc:write("editor = " .. profile["setupconf_editor"] .. "\n\n")

    -- EXTENSIONS section
    hgrc:write([[
[extensions]
mq =
graphlog =
color =
extdiff =
pager =
purge =
rebase =
prompt = ]] .. pack_path .. sep .. "etc" .. sep .. "prompt.py")

    -- EXTDIFF section
    hgrc:write([[


[extdiff]
cmd.kdiff3 = kdiff3
cmd.kompare = kompare

]])

    -- PAGER section
    hgrc:write([[
[pager]
pager = less -FSRXe

]])

    -- ALIAS section
    hgrc:write("[alias]\n")
    hgrc:write("mrproper = revert --no-backup --all\n\n")

    io.close(hgrc)
  end
}
