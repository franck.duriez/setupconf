
return {
  depends = "shell",
  link = {
    ["$PACROOT/etc/zshrc"]   = "$HOME/.zshrc",
    ["$PACROOT/etc/zshrc.d"] = "$HOME/.zshrc.d",
  }
}
