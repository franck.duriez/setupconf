#!/bin/sh

autoload -Uz zgitinit && zgitinit
autoload -Uz colors   && colors
autoload -Uz vcs_info
autoload -Uz add-zsh-hook

####################################
# COLORS
####################################

bright="\033[m1;"
dull="\033[m0;"
for color in BLACK RED GREEN YELLOW BLUE MAGENTA CYAN WHITE; do
  eval PR_$color='%{$fg_no_bold[${(L)color}]%}'
  eval PR_BG_$color='%{$bg[${(L)color}]%}'
  eval PR_LIGHT_$color='%{$fg_bold[${(L)color}]%}'
  (( count = $count + 1 ))
done
PR_NO_COLOUR="%{$terminfo[sgr0]%}"

#customise this two variable to change foreground/background
#left empty for automatic default
PR_BG_PROMPT=""
PR_FG_PROMPT="${PR_NO_COLOUR}"

####################################
# VCS_INFO
####################################

#may be slow
zstyle ':vcs_info:*:setup:*' check-for-changes false
#may be slow
zstyle ':vcs_info:*:setup:*' get-revision      false
zstyle ':vcs_info:*:setup:*' use-simple        false
zstyle ':vcs_info:*:setup:*' max-exports       2

# set formats
# %b - branchname
# %u - unstagedstr (see below)
# %c - stangedstr (see below)
# %a - action (e.g. rebase-i)
# %R - repository path
# %S - path in the repository
#default format for standard vcs
ACTION="[${PR_LIGHT_RED}%a${PR_FG_PROMPT}]"
FORMAT="%s on ${PR_MAGENTA}%b${PR_FG_PROMPT}"
FORMAT_ACTION="$FORMAT $ACTION"
zstyle ':vcs_info:*:setup:*' formats             "$FORMAT" "%s"
zstyle ':vcs_info:*:setup:*' actionformats       "$FORMAT_ACTION" "%s"
zstyle ':vcs_info:*:setup:*' branchformat        "${PR_MAGENTA}%b${PR_FG_PROMPT}:${PR_LIGHT_RED}%r${PR_NO_COLOUR}"

prompt_setup_git_tracking_branch()
{
  t=$(zgit_tracking_merge)
  if ! [ "$t" = "" ] ; then
    echo " tracking${PR_LIGHT_MAGENTA} ${t}${PR_FG_PROMPT}"
  fi
}

#format vcs information, append information to vcs_info that is a little poor
#this function is called each time a prompt need to be display
#this function set the psvar with changing information
prompt_setup_precmd() {
  setopt noxtrace noksharrays localoptions
  #if the function is not found, fail silently (degraded mode)
  vcs_info 'setup' 2>/dev/null
  psvar=()
  case "$vcs_info_msg_1_" in
    git)
      echo $setupconf_packages | grep "git" &>/dev/null && \
      GIT_FORMAT="$vcs_info_msg_0_ (${PR_LIGHT_YELLOW}$(git describe --always --tags 2>/dev/null)${PR_FG_PROMPT})$(prompt_setup_git_tracking_branch 2>/dev/null)" && \
      psvar[1]="[$GIT_FORMAT]"
      ;;
    git-svn)
      echo $setupconf_packages | grep "git" &>/dev/null && \
      GITSVN_FORMAT="$vcs_info_msg_0_ (${PR_LIGHT_YELLOW}$(git describe --always --tags) ${PR_MAGENTA}$(zgit_svnbranch)${PR_FG_PROMPT}:${PR_LIGHT_RED}$(zgit_svnhead)${PR_FG_PROMPT})$(prompt_setup_git_tracking_branch)" && \
      psvar[1]="[$GITSVN_FORMAT]"
      ;;
    #hg)
    #  echo $setupconf_packages | grep "mercurial" &>/dev/null && \
    #  MERCURIAL_FORMAT="$vcs_info_msg_0_ ($(hg prompt "{incoming changes {incoming}}{patches|join( > )|pre_applied([4m)|post_applied([0m)|pre_unapplied([2m)|post_unapplied([0m)}{ outgoing changes{outgoing}}"))" && \
    #  psvar[1]="[$MERCURIAL_FORMAT]"
    #  ;;
    "")
      #no vcs info => dont do anything
      ;;
    *)
      psvar[1]="[$vcs_info_msg_0_]"
      ;;
  esac
  if [ "$psvar[1]" != "" ] ; then
    psvar[1]="${PR_BG_PROMPT}${PR_FG_PROMPT}$psvar[1]${PR_NO_COLOUR}
"
  fi
  psvar[2]=""
  if ! [ -z "$SETUPCONF_ENV" ] ; then
    psvar[2]="${PR_NO_COLOUR}<${PR_LIGHT_RED}${SETUPCONF_ENV}${PR_NO_COLOUR}>"
  fi
  if ! [ -z "$SCHROOT_COMMAND" ] ; then
    psvar[2]="${PR_LIGHT_YELLOW}:SCHROOT${PR_NO_COLOUR}$psvar[2]"
  fi
}


prompt_basic_setup () {
  add-zsh-hook precmd prompt_setup_precmd 2>/dev/null
  BASE_PROMPT="${PR_BG_PROMPT}${PR_LIGHT_MAGENTA}%(!.%SROOT%s.${PR_FG_PROMPT}%n${PR_LIGHT_YELLOW})\
@${PR_LIGHT_GREEN}%m\$psvar[2]${PR_LIGHT_MAGENTA} \
[%(?.${PR_FG_PROMPT}$?.${PR_LIGHT_RED}% err %?)${PR_LIGHT_MAGENTA}] \
[${PR_LIGHT_BLUE}%~${PR_LIGHT_MAGENTA}]${PR_NO_COLOUR}
${PR_BG_PROMPT}${PR_FG_PROMPT}z${PR_LIGHT_GREEN}%(!.#.$)${PR_NO_COLOUR} "

  PROMPT="\$psvar[1]${BASE_PROMPT}"
  RPROMPT="[${PR_BG_PROMPT}${PR_LIGHT_CYAN}%D ${PR_LIGHT_GREEN}%T${PR_NO_COLOUR}]"

  PS2=''
}

prompt_basic_setup "$@"

