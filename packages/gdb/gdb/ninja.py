#!/bin/env python3

##
## Original source at https://github.com/bbguimaraes/gdb_lua
## Documentation at https://bbguimaraes.com/gdb_lua/
##

import itertools
import re
import sys
import typing
import subprocess

import gdb

HELP_NINJA = 'Commands to run ninja build system.'


def cmd_ninja(_, arg: str, _from_tty):
    args = gdb.string_to_argv(arg)
    subprocess.run(["ninja", *args])


def make_command(
    doc: str,
    invoke: typing.Optional[typing.Callable],
    *args, **kwargs,
) -> gdb.Command:
    class Command(gdb.Command):
        def __init__(self):
            super(Command, self).__init__(*args, **kwargs)
    Command.__doc__ = doc
    if invoke:
        Command.invoke = invoke
    Command()


if __name__ == '__main__':
    make_command(HELP_NINJA, cmd_ninja, 'ninja', gdb.COMMAND_RUNNING, prefix=True)
