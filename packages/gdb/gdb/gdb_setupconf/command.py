

def split(txt):
  """
  Split command line input as a shell does.
  """
  words = []
  prev = 0
  quot = None
  escape = False
  current = ""
  for i in range(len(txt)):
    if escape:
      current += txt[i]
      escape   = False
    elif txt[i] == '\\':
      escape = True
    elif txt[i] in [ '"', "'" ]:
      if not quot: quot = txt[i]
      elif quot != txt[i]: current += txt[i]
    elif txt[i] == ' ':
      if quot: current += txt[i]
      elif current:
        words.append(current)
        current = ""
    else: current += txt[i]

  if current != "": words.append(current)
  return words

def complete_from_subcommand_list(subcommands):
  """

  """
  def decorator(func):
    def wrapper(self, current, txt):
      words = split(txt.replace('\x00', ''))

      if current.startswith(words[-1]):
        words = [words[0]] + split(current)

      # This is the first word entered.
      if len(words) <= 1 or words[1] == current:
        return filter(lambda x: x.startswith(current), subcommands)

      # Else call complete function of the matching keyword.
      to_call = getattr(self, "%s_complete" % words[1], None)
      if to_call: return to_call("" if current.endswith(" ") else words[-1], words[1:])
      return []

    return wrapper
  return decorator