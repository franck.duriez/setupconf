import gdb

import gdb_setupconf.command


class BreakBacktraceContinue(gdb.Command):

  class BreakPoint(gdb.Breakpoint):

    def __init__(self, words):
      super(BreakBacktraceContinue.BreakPoint, self).__init__(words[0])
      self.words = words

    def check_if_statement(self):
      if len(self.words) < 3 or self.words[1] != "if":
        return False

      to_eval = " ".join(self.words[2:])
      value   = gdb.parse_and_eval(to_eval)
      return value

    def stop(self):

      if len(self.words) < 2 or self.check_if_statement():
        if self.thread:   print("## Backtrace from thread %s" % self.thread)
        else:             print("## Backtrace")
        if self.location: print("## On location \"%s\"" % self.location)

        gdb.execute("backtrace")

      return False


  def __init__(self, name):
    super(BreakBacktraceContinue, self).__init__(name, gdb.COMMAND_OBSCURE)

  def invoke(self, txt, from_tty):
    words = gdb_setupconf.command.split(txt)
    self.__class__.BreakPoint(words)

  def complete(self, current, txt):
    return gdb.COMPLETE_LOCATION


BreakBacktraceContinue("break_backtrace_continue")
BreakBacktraceContinue("bbc")
