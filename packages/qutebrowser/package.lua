
return {
  link = {
    ["$PACROOT/etc/config.py"]   = "$HOME/.config/qutebrowser/config.py",
    ["$PACROOT/etc/bookmarks"]   = "$HOME/.config/qutebrowser/bookmarks",
  }
}
