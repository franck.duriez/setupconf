local installer = require("setupconf.os.installer")

return {
  run = function(conf)
    installer.install({ "git", "zsh", "gvim", "vim-runtime" })
  end
}
