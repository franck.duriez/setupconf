local scos = require("setupconf.os")

return {
  link = {
    ["$PACROOT/etc"] = "$HOME/.config/git",
  },
  run = function(profile)
    -- User
    local name = profile["setupconf_name"]
    local mail = profile["setupconf_mail"]
    local editor = profile["setupconf_editor"]

    assert(name, "user profile must contain setupconf_name")
    assert(mail, "user profile must contain setupconf_mail")

    if not scos.which("git")
    then
      print("!! git is not installed on the system...")
      return
    end

    os.execute("git config --global user.name '" .. name .. "'")
    os.execute("git config --global user.email '" .. mail .. "'")

    -- Config
    os.execute("git config --global diff.tool meld")
    os.execute("git config --global diff.renameLimit 30000")
    os.execute("git config --global pull.rebase true")
    os.execute("git config --global init.defaultBranch master")
    os.execute("git config --global gui.tabsize 2")
    os.execute("git config --global color.ui auto")
    os.execute("git config --global core.editor '" .. editor .. "'")

    -- Aliases
    os.execute("git config --global alias.graph \"log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative\"")
    os.execute("git config --global alias.short 'log --oneline'")
    os.execute("git config --global alias.suir 'submodule update --init --recursive'")
    os.execute("git config --global alias.chicken 'pull --rebase'")

    -- Diff tools
    os.execute("git config --global diff.zip.textconv 'unzip -v'")
    os.execute("git config --global diff.pdf.textconv 'pdfinfo'")
    os.execute("git config --global diff.bin.textconv 'hexdump -v -C'")
    os.execute("git config --global diff.hex.textconv 'hexdump -v -C'")
    os.execute("git config --global diff.hex.binary true")
  end,
}
