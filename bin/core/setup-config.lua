local sc     = require("setupconf")
local scpack = require("setupconf.package")
local color  = require("setupconf.colorize")
local logger = require("setupconf.log")

local ret, parsed = sc.parse_profile()
if not ret
then
  -- Create default user profile file.
  parsed = sc.create_default_profile()
  logger.info("Please fill the file:")
  logger.info(sc.profile_path)
  logger.info("with your personnal setiings and relaunch the command.")
  return
end

logger.info(color.colorize("%{bold:blue}Processing active packages...%{clear}"))
logger.indent.increase()

local active_packages =
  scpack.get_active_ones(parsed["setupconf_packages"])

for package_name in scpack.list()
do
  if active_packages[package_name]
  then
    logger.info(color.colorize("%{bold:blue}Processing%{clear}: %{bold:yellow}" .. package_name .. "%{clear}"))
    logger.indent.increase()
    scpack.setup(package_name, parsed)
    logger.indent.decrease()
  else
    logger.info(color.colorize("%{red}Skipping%{clear} (inactive): %{bold:black}" .. package_name .. "%{clear}"))
  end
end

logger.indent.decrease()
logger.info(color.colorize("%{blue}Package processing terminated.%{clear}"))