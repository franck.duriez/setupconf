local scstr = require("setupconf.string")

local TEST = {}

TEST.interpret_shell = function()
  local res = scstr.interpret_shell("$HOME/.config")
  return res == os.getenv("HOME") .. "/.config"
end

return TEST
