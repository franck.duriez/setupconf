local scos = require("setupconf.os")

local TEST = {}

local tmpdir = "/tmp/setupconf-test/" .. math.random()

TEST.init = function()
  os.execute("mkdir -p " .. tmpdir)
end

TEST.finalize = function()
  os.execute("rm -fr " .. tmpdir)
end

TEST.ln_link_correctly = function()
  local src = tmpdir .. "/toto"
  local dst = tmpdir .. "/titi"
  os.execute("touch " .. src)
  scos.ln(src, dst)
  local result
  for line in io.popen("readlink -f " .. dst):lines()
  do
    result = line
  end
  os.remove(src)
  os.remove(dst)
  return result and result == src
end

TEST.ln_override_existing_link = function()
  local src1 = tmpdir .. "/toto"
  local src2 = tmpdir .. "/tutu"
  local dst = tmpdir .. "/titi"
  os.execute("touch " .. src1)
  os.execute("touch " .. src2)
  scos.ln(src1, dst)
  scos.ln(src2, dst)
  local result
  for line in io.popen("readlink -f " .. dst):lines()
  do
    result = line
  end
  os.remove(src1)
  os.remove(src2)
  os.remove(dst)
  return result and result == src2
end

TEST.is_dir_good_result = function()
  return scos.is_dir(tmpdir)
end

return TEST
